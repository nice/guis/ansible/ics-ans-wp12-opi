import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]).get_hosts("wp12_opi")


@pytest.mark.parametrize("file", [("/etc/profile.d/nss_opi.sh")])
def test_files(host, file):
    file = host.file(file)
    assert file.exists
